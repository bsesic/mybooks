from django.apps import AppConfig


class MybooksappConfig(AppConfig):
    name = 'mybooksapp'
